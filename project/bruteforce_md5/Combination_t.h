/* 
 * File:   Combination_t.h
 * Author: lemz
 *
 * Created on September 14, 2016, 2:32 PM
 */

#ifndef COMBINATION_T_H
#define	COMBINATION_T_H

#include <vector>

class Combination_t {
public:
    Combination_t();
    
    Combination_t(unsigned int min, unsigned int max);
    
    void set_charset (const std::vector <bool> & ascii_char_set);
    
    unsigned long combination_size ();
    
    ~Combination_t();
private:
    unsigned int min = 0;
    unsigned int max = 0;
    
    std::vector < std::vector < bool > > data;
    
};

#endif	/* COMBINATION_T_H */

