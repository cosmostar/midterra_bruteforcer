/* 
 * File:   Combination_t.cpp
 * Author: lemz
 * 
 * Created on September 14, 2016, 2:32 PM
 */

#include "Combination_t.h"

Combination_t::Combination_t() {
}


Combination_t::Combination_t(unsigned int min, unsigned int max) : min(min), max(max)
{
    std::vector <bool> vec;
    vec.resize(256,false);
    this->data.resize(max,vec );
}

Combination_t::~Combination_t() {
}

void Combination_t::set_charset (const std::vector <bool> & ascii_char_set)
{
    for ( auto i : this->data)
    {
        i = ascii_char_set;
    }

}

 unsigned long Combination_t::combination_size ()
 {
     unsigned long sz =0 ;
     
        for ( auto i : this->data)
    {
            unsigned int count = 0;
            for (auto bit : i)
                if (bit)
                    count++;
            
        sz ? sz = sz  * count : sz = count;
    }
     
     
 }
    