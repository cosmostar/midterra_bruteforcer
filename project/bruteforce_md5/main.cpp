/* 
 * File:   main.cpp
 * Author: lemz
 *
 * Created on September 14, 2016, 2:30 PM
 */

#define _faster

#include <cstdlib>
#include "char_domain.h"
#include "../../md5.h"

#include <thread>
#include <future>
#include <condition_variable>
#include <mutex>
#include <chrono>
#include <iostream>

/*
 *
 */


class task {
public:

    task(const task &) {

    }

    static std::string success_str;

    task() {

        std::unique_lock<std::mutex> lk(task::mutex_data);
        vec.push_back(this);

        thread = new std::thread(&task::process, this);

    }

    void join() {
        thread->join();
    }

    static task* wait_for_ready() {
        std::unique_lock<std::mutex> lk(task::mutex_data);
        for (auto i : vec) {
            std::unique_lock<std::mutex> lk1(i->mutex_ready_to_start);
            if (!i->charged) {
                return i;
            }


        }
        cv_ready_to_start.wait(lk);
        for (auto i : vec) {
            std::unique_lock<std::mutex> lk1(i->mutex_ready_to_start);
            if (!i->charged) {
                return i;
            }

        }

    }
    void deblock (void)
    {
        this->work_flag =false;
    cv_completed.notify_all();
    }

    void charge(char_domain * dm, unsigned long start, unsigned long end, unsigned int str_size) {

        std::unique_lock<std::mutex> lk(this->mutex_ready_to_start);
        {
            if (!this->charged) {
                this->domain = dm;
                this->start = start;
                this->end = end;
                this->sz = str_size;

                this->charged = true;

                lk.unlock();

                cv_completed.notify_one();
            }

        }
    }

    static bool wait_and_charge(char_domain * dm, unsigned long start, unsigned long end, unsigned int str_size) {
        std::unique_lock<std::mutex> lk(task::mutex_data);
        for (auto i : vec) {
            std::unique_lock<std::mutex> lk1(i->mutex_ready_to_start);
            {
                if (task::found)
                    return true;

                if (!i->charged) {
                    i->domain = dm;
                    i->start = start;
                    i->end = end;
                    i->sz = str_size;

                    i->charged = true;

                    lk1.unlock();

                    i->cv_completed.notify_one();
                }

            }

        }
        cv_ready_to_start.wait(lk);
        for (auto i : vec) {
            std::unique_lock<std::mutex> lk1(i->mutex_ready_to_start);
            {
                if (task::found)
                    return true;
                if (!i->charged) {
                    i->domain = dm;
                    i->start = start;
                    i->end = end;
                    i->sz = str_size;

                    i->charged = true;

                    lk1.unlock();

                    i->cv_completed.notify_one();
                }

            }

        }

        return false;
    }

    uint8_t target_hash [16];
    static bool found;

private:
    static std::vector <task * > vec;



    static std::mutex mutex_data;
    static std::condition_variable cv_ready_to_start;

    void process() {
        MD5 md5;

        char str[256];
        while (this->work_flag) {

            {
                std::unique_lock<std::mutex> lk(this->mutex_ready_to_start);
              
                if (!this->charged) {
                    this->cv_completed.wait(lk);
                }
                  if (!this->work_flag)          
                    break;
            }



            for (unsigned long i = this->start; i < this->end; i++) {

                md5.digestString(this->domain->get_string(str, sizeof (str),
                        i, this->sz));


                if (!memcmp(md5.digestRaw, this->target_hash, sizeof (this->target_hash))) {
                    task::mutex_data.lock();
                    this->found = true;
                    success_str = this->domain->get_string(str, sizeof (str),
                            i, this->sz);
                    this->mutex_ready_to_start.unlock();

                    this->cv_ready_to_start.notify_all();
                    task::mutex_data.unlock();
                }

            }

            task::mutex_data.lock();
            this->mutex_ready_to_start.lock();
            this->charged = false;
            this->mutex_ready_to_start.unlock();
            task::mutex_data.unlock();

            this->cv_ready_to_start.notify_all();


        }

    }

    std::thread * thread;

    bool charged = false;

    char_domain * domain;

    size_t sz = 0;
    unsigned long start = 0;
    unsigned long end = 0;

    std::condition_variable cv_completed;

    std::mutex mutex_ready_to_start;

    

    unsigned char * value_to_compare;

    bool work_flag =true;


};

std::vector <task * > task::vec;

std::mutex task::mutex_data;

std::condition_variable task::cv_ready_to_start;


std::string task::success_str;

bool task::found = false;

unsigned long pow_long(unsigned int base, unsigned int exp) {
    if (exp == 0)
        return 1;
    exp--;
    unsigned long result = base;
    while (exp--) {
        result = result *base;

    }
    return result;
}

int main(int argc, char** argv) {

    if (argc != 5)
        printf("4(four) argument must be  here");

    unsigned int min, max;

    min = atoi(argv[1]);
    max = atoi(argv[2]);

    
    uint8_t target_hash [16];

    int d = 0;
    for (int i = strlen(argv[4])-1; i >= 0; i--) {
        char t;
        char & c = argv[4][i ];

        if (('0' <= c) && ('9' >= c)) {
            t = c - '0';
        } else
            if (('a' <= c) && ('f' >= c)) {
            t = (c - 'a') + 10;
        } else
            if (('A' <= c) && ('F' >= c)) {
            t = c - 'A' + 10;
        } else
            if ((c == 'x') || c == 'X') {
        } else {
            printf("failed to parse hash string\n");
            return 1;
        }
        if ((i) % 2 != 0) {
            target_hash[(i) / 2] = (t & 0xff);
        } else {
            target_hash[(i) / 2] = target_hash[(i)/ 2] | (t & 0xff) << 4;
        }
        d++;
    }


    char_domain domain;
    
    domain.parse_domain(argv[3]);

    for (char c = 'a'; c <= 'z'; c++)
        domain.add_letter(c);


    unsigned int thread_count = std::thread::hardware_concurrency();



    std::vector <task> tsk;

    tsk.resize(thread_count);

    for (task & i : tsk) {
        memcpy(i.target_hash, target_hash, sizeof (target_hash));
    }



    unsigned int package_sz = 10000;

    auto start = std::chrono::high_resolution_clock::now();

    unsigned long job = 0;

    for (int letter_s_count = min; letter_s_count <= max; letter_s_count++) {

        unsigned long all_comb = pow_long(domain.get_combination_size(), letter_s_count);

        job = job + all_comb;

        for (unsigned long l = 0; l < all_comb;) {

            unsigned long next = (package_sz > all_comb - l) ? all_comb - l : package_sz;
            if (task::wait_and_charge(&domain, l, l + next, letter_s_count))
                goto exit;

            l = l + next;



        }

    }
exit:
    for (task & i : tsk) {
        i.deblock();
    }
              
            

    for (task & i : tsk) {
        i.join();
    }

    if (task::found) 
    {
        printf("hash is %s\n", task::success_str.c_str());
    }
    else
    {
        printf("hash not found\n");
    }



    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << job / diff.count() << " hashes per second" << std::endl;



    return 0;



}

