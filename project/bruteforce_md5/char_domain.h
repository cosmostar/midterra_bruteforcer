/* 
 * File:   char_domain.h
 * Author: lemz
 *
 * Created on September 14, 2016, 2:56 PM
 */

#ifndef CHAR_DOMAIN_H
#define	CHAR_DOMAIN_H

#include <vector>
#include <string>

class char_domain : private std::vector<bool>  {
public:
    char_domain();
    
    char get_letter_num (unsigned int) ;
    unsigned long get_combination_size()  ;
    
    std::string get_string(unsigned long num,unsigned int str_size);
    char *get_string (char * p,size_t n,unsigned long num,unsigned int str_size);
    
    void add_letter (char c);
    bool parse_domain ( char * str);
    
private:
    
    bool dirty_flag= false;
    std::vector <char > cache;
 
};

#endif	/* CHAR_DOMAIN_H */

