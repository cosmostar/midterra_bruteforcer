/* 
 * File:   char_domain.cpp
 * Author: lemz
 * 
 * Created on September 14, 2016, 2:56 PM
 */

#include "char_domain.h"
#include <cmath>
#include <iostream>
#include <iterator>
#include <string>


//#include <regex>
// gcc 4.8 has no support of regex


char_domain::char_domain() {

    this->resize(256, false);
}

unsigned long char_domain::get_combination_size() {
    if (this->dirty_flag) {
        this->cache.clear();

        for (size_t i = 0; i < this->size(); i++) {


            if (this->at(i))
                this->cache.push_back(i);
        }
        this->dirty_flag = false;
    }


    return this->cache.size();
#if 0
    unsigned int count = 0;
    for (size_t i = 0; i < this->size(); i++) {
        if (this->at(i))
            count++;
    }
    return count;
#endif
}

char char_domain::get_letter_num(unsigned int t) {
    if (this->dirty_flag) {
        this->cache.clear();

        for (size_t i = 0; i < this->size(); i++) {


            if (this->at(i))
                this->cache.push_back(i);
        }
        this->dirty_flag = false;
    }

    return this->cache.at(t);
#if 0
    int count = -1;
    for (size_t i = 0; i < this->size(); i++) {


        if (this->at(i))
            count++;
        if (count == t)
            return i;


    }
#endif
}

void char_domain::add_letter(char c) {
    this->at(reinterpret_cast<unsigned char &> (c)) = true;
    this->dirty_flag = true;
}



unsigned long pow_long(unsigned int base, unsigned int exp);

std::string char_domain::get_string(unsigned long num, unsigned int str_size) {
    unsigned long comb = get_combination_size();
    unsigned long all_comb = comb;

    for (int i = str_size; i > 1; i--) {
        all_comb = comb*comb;
    }

    std::string result;

    for (int i = 0; i < str_size; i++) {
        unsigned long a = num % pow_long(comb, str_size - i) / pow_long(comb, str_size - i - 1);
        result.push_back(get_letter_num(a));

        num = num - a * pow_long(comb, str_size - i);
    }

    return result;
}

char *char_domain::get_string(char *p, size_t n, unsigned long num, unsigned int str_size) {

    unsigned long comb = get_combination_size();
    unsigned long all_comb = comb;

    for (int i = str_size; i > 1; i--) {
        all_comb = comb*comb;
    }

    std::string result;
    int i;
    for (i = 0; i < str_size && i < (n - 1); i++) {
        unsigned long a = num % pow_long(comb, str_size - i) / pow_long(comb, str_size - i - 1);
        p[i] = (get_letter_num(a));

        num = num - a * pow_long(comb, str_size - i);
    }
    p[i + 1] = 0;

    return p;
}


std::vector<std::string> split(const std::string& input, const char & c) 
{
    std::vector<std::string> result;
    
    result.resize(1);
    
    for ( char i : input)
    {
        if ( i == c)
            result.push_back(std::string());
        else
            result[result.size()-1].push_back(i);
    }
    return result;
}

bool char_domain::parse_domain(char * str) {
    std::vector <std::string> str_list;

    str_list.resize(1);

    for (size_t i = 0; str[i]; i++) {
        char & c = str[i];
        if (c == ':')
            if (str_list.empty()) {
                printf("failed to parse char domain\n");
                return false;
            } else {
                str_list.push_back(std::string());
            }

        else if (((c >= '0') && (c <= '9')) || c == ('-')) {
            str_list[str_list.size() - 1].push_back(c);
        } else {
            printf("unknown symbol %c\n", c);
        }
    }
    
    for ( std::string str : str_list)
    {
       std::vector <std::string>  str_list2 =   split(str,'-');
       
       int min,max;
       
       if (str_list.size() == 2)
       {
           min =  atoi (str_list.at(0).c_str());
           max =  atoi (str_list.at(1).c_str());
           for (unsigned  char c = min;  c < max; c++ )
               this->add_letter(c);
       }
       
          if (str_list.size() == 1)
       {
           min =  atoi (str_list.at(0).c_str());
           this->add_letter(static_cast <char>(min));
       }
       
    }

}

