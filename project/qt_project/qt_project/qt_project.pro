TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ../../bruteforce_md5/char_domain.cpp \
    ../../bruteforce_md5/Combination_t.cpp \
    ../../bruteforce_md5/main.cpp

HEADERS += \
    ../../bruteforce_md5/char_domain.h \
    ../../bruteforce_md5/Combination_t.h \
    ../../../md5.h


LIBS += -pthread
