default : md5.h ./project/bruteforce_md5/main.cpp  ./project/bruteforce_md5/char_domain.cpp ./project/bruteforce_md5/char_domain.h
	g++ -std=c++11 -pthread -o md5_bruteforce md5.h ./project/bruteforce_md5/main.cpp  ./project/bruteforce_md5/char_domain.cpp ./project/bruteforce_md5/char_domain.h

all : test default

test: md5.h  md5_lib_test.cpp
	g++ -O3 -std=c++11 md5_lib_test.cpp -o md5strcalc

